#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24


void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size
size_from_capacity(block_capacity
                   cap);

extern inline block_capacity
capacity_from_size(block_size
                   sz);

bool block_is_big_enough(size_t query, struct block_header *block) {
    return block->capacity.bytes >= query;
}

size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next, .capacity = capacity_from_size(block_sz), .is_free = true};
}

static size_t region_actual_size(size_t query) {
    return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool

region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    query = region_actual_size(query);
    struct region region = {map_pages(addr, query, MAP_FIXED), query, true};
    if (region.addr == MAP_FAILED) {
        region.addr = map_pages(addr, query, 0);
        region.extends = false;
        if (region.addr == MAP_FAILED)return REGION_INVALID;
    }
    block_init(region.addr, size_from_capacity((block_capacity) {query}), NULL);
    return region;
}

static void *block_after(struct block_header const *block);

bool heap_inited = false;

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region))
        return NULL;
    heap_inited = true;
    return region.addr;
}


/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block,
                             size_t query) {
    return block->is_free &&
           query + offsetof(
                   struct block_header, contents) + BLOCK_MIN_CAPACITY <=
           block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (!block_splittable(block, query)) {
        return false;
    }
    query = query > BLOCK_MIN_CAPACITY ? query : BLOCK_MIN_CAPACITY;
    block_init(block->contents + query, (block_size) {block->capacity.bytes - query}, block->next);

    block_init(block, (block_size) {query + offsetof(struct block_header, contents)}, block->contents + query);
    block->is_free = false;

    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(struct block_header const *fst,
                              struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return  !fst && !snd && fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    if (!mergeable(block, block->next)) {
        return false;
    }
    block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
    block->next = block->next->next;
    return true;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    if (!block || block==block->next)return (struct block_search_result) {BSR_CORRUPTED, NULL};
    struct block_search_result result = {BSR_FOUND_GOOD_BLOCK, block};
    while (result.block && result.block->next) {
        while (try_merge_with_next(result.block));

        if (block_is_big_enough(sz, result.block) && result.block->is_free) {
            split_if_too_big(result.block, sz);
            return result;
        }
        result.block = result.block->next;
    }
    result.type = BSR_REACHED_END_NOT_FOUND;
    return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь
 расширить кучу Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result result = find_good_or_last(block, query);
    if (result.type == BSR_FOUND_GOOD_BLOCK)result.block->is_free = false;
    return result;
}

static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    if (!last || !last->is_free )return NULL;
    const struct region region = alloc_region(block_after(last), query);
    if(!region.extends)return NULL;
    last->next = (struct block_header *) region.addr;
    return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    struct block_search_result result = try_memalloc_existing(query, heap_start);
    if (result.type == BSR_REACHED_END_NOT_FOUND) {
        if(!grow_heap(result.block, query))return NULL;
        result = try_memalloc_existing(query, result.block);
    }
    return result.block;
}

void *_malloc(size_t query) {
    if (!heap_inited)heap_init(query);
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr)
        return addr->contents;
    else
        return NULL;
}

struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(
            struct block_header, contents));
}

void _free(void *mem) {
    if (!mem)
        return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}
