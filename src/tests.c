#include "tests.h"


void test1(FILE *result) {
    void *test1 = _malloc(12), *test2 = _malloc(25), *test3 = _malloc(100);

    struct block_header *block1 = block_get_header(test1), *block2 = block_get_header(
            test2), *block3 = block_get_header(test3);
    debug_heap(result, block1);
    if (block1->capacity.bytes == 24 && block2->capacity.bytes == 25 && block3->capacity.bytes == 100) {
        fprintf(result, "\nMemory allocated successfully\n\n");
    } else {
        fprintf(result, "\nMemory allocated failly\n\n");
    }
    _free(test1);
    _free(test2);
    _free(test3);

}

void test2(FILE *result) {
    void *test1 = _malloc(11), *test2 = _malloc(25), *test3 = _malloc(100);

    struct block_header *block1 = block_get_header(test1), *block2 = block_get_header(
            test2), *block3 = block_get_header(test3);


    _free(test2);

    debug_heap(result, block1);
    if (block1->capacity.bytes == 24 && block3->capacity.bytes == 100 && block2->is_free == true) {
        fprintf(result, "\nBlocks freed successfully\n\n");
    } else {
        fprintf(result, "\nBlocks freed failly\n\n");
    }
    _free(test1);
    _free(test3);
}


void test3(FILE *result) {
    void *test1 = _malloc(11), *test2 = _malloc(25), *test3 = _malloc(100), *test4 = _malloc(1234);

    struct block_header *block1 = block_get_header(test1), *block2 = block_get_header(
            test2), *block3 = block_get_header(test3), *block4 = block_get_header(test4);

    _free(test2);
    _free(test3);
    debug_heap(result, block1);
    if (block1->capacity.bytes == 24 && block3->capacity.bytes == 100 && block2->is_free == true && block3->is_free) {
        fprintf(result, "\nBlocks freed successfully\n\n");
    } else {
        fprintf(result, "\nBlocks freed failly\n\n");
    }
    _free(test1);
    _free(test4);
}

void test4(FILE *result) {
    void *test1 = _malloc(8192), *test2 = _malloc(25), *test3 = _malloc(100);

    struct block_header *block1 = block_get_header(test1), *block2 = block_get_header(
            test2), *block3 = block_get_header(test3);
    debug_heap(result, block1);
    if (block1->capacity.bytes == 8192 && block2->capacity.bytes == 25 && block3->capacity.bytes == 100) {
        fprintf(result, "\nMemory allocated successfully\n\n");
    } else {
        fprintf(result, "\nMemory allocated failly\n\n");
    }
    _free(test1);
    _free(test2);
    _free(test3);

}

void test5(FILE* result){
  // struct region region=alloc_region((HEAP_START+4096), 8192);
  // ((struct block_header*)region.addr)->is_free=false;
  // debug_heap(result, region.addr);
  // void* test=_malloc(9000);
  // debug_heap(result, block_get_header(test));
  }
void all_test(FILE* result){
  test1(result);
  test2(result);
  test3(result);
  test4(result);
  test5(result);
}


